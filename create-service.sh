#!/bin/bash
#docker swarm init
#docker network create -d overlay --attachable mqtt-network

docker service create  \
  --replicas 3 --network mqtt-network \
  --env HIVEMQ_DNS_DISCOVERY_ADDRESS=tasks.hivemq \
  --publish target=1883,published=1883 \
  --publish target=8080,published=8080 \
  -p 8000:8000/udp \
  --name hivemq \
    hivemq/hivemq3:dns-latest
